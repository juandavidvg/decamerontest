<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Hotel
 *
 * @author jdvasquez
 */
class Hotel extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("Hotel_model", "hotel");
    }

    /**
     * Metodo para cargar las vista de creacion hotel
     * @access public
     * @author Juan Vasquez <juandavid.vasquezg@gmail.com>
     */
    public function view_load_hotel_creation() {
        $this->load->view('header.html');
        $this->load->view('hoteles/create-hotel.html');
        $this->load->view('footer.html');
    }

    /**
     * Metodo para cargar las vista de creacion de habitacion
     * @access public
     * @author Juan Vasquez <juandavid.vasquezg@gmail.com>
     */
    public function view_load_hotel_habitacion_create() {
        $this->load->view('header.html');
        $this->load->view('hoteles/create_habitacion.html');
        $this->load->view('footer.html');
    }

    /**
     * Metodo para obtener las ciudades de donde se puede crear un hotel decameron
     * @access public
     * @author Juan Vasquez <juandavid.vasquezg@gmail.com>
     */
    public function getDataCiudad() {

        # Compruebo que la petición sea de ajax
        if ($this->input->is_ajax_request()) {
            # Obtengo los datos de los tipos de documentos
            $ciudades = $this->hotel->getCiudadesDecameron();

            $data = array(
                'datos' => $ciudades
            );

            header('Content-type: application/json; charset=utf-8');
            echo json_encode($data, JSON_PARTIAL_OUTPUT_ON_ERROR);
        }
    }

    /**
     * Metodo para crear una solicitud de un convenio
     * @author vasquez.juam@locatelcolombia.complete
     * @access public
     */
    public function setDataHotel() {

        # Array de respúesta
        $array_return = array('msj' => '', 'status' => false);

        # Objeto de la petición con todos los input
        $data = json_decode(base64_decode($_POST['data']));

        $nom_hotel = strtoupper(trim($data->nom_hotel));
        $nit_hotel = trim($data->nit_hotel);
        $cod_ciu = $data->cod_ciu;
        $num_hab = $data->num_hab;
        $con_dir = trim($data->con_dir);

        $exists_hotel = $this->hotel->validate_hotel($nom_hotel, $nit_hotel);

        $array_return['msj'] = 'Hotel ya existe registrado con los datos ' . $nom_hotel . ' -  ' . $nit_hotel;

        # Compruebo la existencia del hotel
        if ($exists_hotel === 0) {
            $this->hotel->setNewHotel($cod_ciu, $nom_hotel, $nit_hotel, $num_hab, $con_dir);
            $array_return['msj'] = 'Se creo hotel correctamente';
            $array_return['status'] = true;
        }

        echo json_encode($array_return);
    }

    public function getDataHotel() {
        # Compruebo que la petición sea de ajax
        if ($this->input->is_ajax_request()) {
            # Obtengo los datos de los tipos de documentos
            $hotel = $this->hotel->getDataHotel();

            $data = array(
                'datos' => $hotel
            );

            header('Content-type: application/json; charset=utf-8');
            echo json_encode($data, JSON_PARTIAL_OUTPUT_ON_ERROR);
        }
    }

    public function getDataHabitacion() {
        # Compruebo que la petición sea de ajax
        if ($this->input->is_ajax_request()) {
            # Obtengo los datos de los tipos de documentos
            $habitacion = $this->hotel->getDataTypeRoom();

            $data = array(
                'datos' => $habitacion
            );

            header('Content-type: application/json; charset=utf-8');
            echo json_encode($data, JSON_PARTIAL_OUTPUT_ON_ERROR);
        }
    }

    public function getDataAcomodacion($id_hab) {
        # Compruebo que la petición sea de ajax
        if ($this->input->is_ajax_request()) {
            # Obtengo los datos de los tipos de documentos
            $acom = $this->hotel->getDataAcomodacion($id_hab);

            $data = array(
                'datos' => $acom
            );

            header('Content-type: application/json; charset=utf-8');
            echo json_encode($data, JSON_PARTIAL_OUTPUT_ON_ERROR);
        }
    }

    public function setDataHabitacion() {

        # Array de respúesta
        $array_return = array('msj' => '', 'status' => false);

        # Objeto de la petición con todos los input
        $data = json_decode(base64_decode($_POST['data']));

        $id_hotel = $data->id_hotel;
        $id_hab = $data->id_hab;
        $id_acom = $data->id_acom;
        $num_hab = $data->num_hab;

        $exists_hotel = $this->hotel->validate_room_hotel($id_hotel, $id_hab, $id_acom);

        $array_return['msj'] = 'Habitacion con esa acomodación creada, no se puede duplicar ';

        # Compruebo la existencia del hotel
        if ($exists_hotel === 0) {
            $this->hotel->createHabitacionHotel($id_hotel, $id_hab, $id_acom, $num_hab);
            $array_return['msj'] = 'Se creo habitacion correctamente';
            $array_return['status'] = true;
        }

        echo json_encode($array_return);
    }

}
