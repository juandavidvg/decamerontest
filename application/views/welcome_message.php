<html>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <center>
                        <h3> Prueba Decameron hoteles</h3>
                    </center>
                </div>
                <div class="col-6">
                    <div class="float-right">
                        <div class="card" style="width: 18rem; height: 18rem;">
                            <div class="card-body">
                                <h5 class="card-title">Crear hoteles</h5>
                                <p class="card-text">En este enlace podrá crear los hoteles de la compañia.</p>
                                <a href="<?= base_url('index.php/view_hotel');?>" class="card-link">Crear hotel</a>
                            </div>
                        </div>                        
                    </div>
                </div>
                <div class="col-6 float-left">
                    <div class="card" style="width: 18rem; height: 18rem;">
                        <div class="card-body">
                            <h5 class="card-title">Crear habitaciones por hotel</h5>
                            <p class="card-text">En este enlace podrá relacionar las habitaciones de los hoteles .</p>
                            <a href="<?= base_url('index.php/view_habitacion');?>" class="card-link">Crear habitaciones de hotel</a>
                        </div>
                    </div>
                </div>
                <!--<div class="col-sm">
                    <div class="card" style="width: 18rem;height: 18rem;">
                        <div class="card-body">
                            <h5 class="card-title">Consulta Hoteles</h5>
                            <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
                            <p class="card-text">Consulta los hoteles de la compañia.</p>
                            <a href="#" class="card-link">Consulta</a>
                        </div>
                    </div>
                </div>-->
            </div>
        </div>
    </body>
</html>