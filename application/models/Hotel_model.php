<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Hotel_model
 *
 * @author jdvasquez
 */
class Hotel_model extends CI_Model {

    /**
     * Metodo para obtener las ciudades
     * @return Array
     */
    public function getCiudadesDecameron() {
        $sql = "SELECT *
                FROM decameron.ciudades
                ORDER BY nom_ciudad;";
        return $this->db->query($sql)->result();
    }

    /**
     * Metodo para validar si el hotel ya fue creado
     * @param type $nom_hotel Nombre del hotel
     * @param type $nit_hotel Nit del hotel
     * @return Integer
     */
    public function validate_hotel($nom_hotel, $nit_hotel) {
        $sql = " SELECT * 
                 FROM decameron.hotel
                 WHERE nom_hotel = '$nom_hotel' OR nit_hotel = '$nit_hotel'";
        return $this->db->query($sql)->num_rows();
    }

    /**
     * Metodo para crear un hotel
     * @param type $id_ciu Id de la ciudad
     * @param type $nom_hotel Nombre del hotel
     * @param type $nit_hotel Nit del hotel
     * @param type $num_hab Numero de habitaciones
     */
    public function setNewHotel($id_ciu, $nom_hotel, $nit_hotel, $num_hab, $dir_hotel) {
        $sql = "INSERT INTO decameron.hotel
                    (id_ciudad,
                    nom_hotel,
                    nit_hotel,
                    num_hab_hotel,
                    dir_hotel)
                    VALUES
                    ($id_ciu,
                    '$nom_hotel',
                    '$nit_hotel',
                    $num_hab,
                    '$dir_hotel');";
        $this->db->query($sql);
    }

    /**
     * Metodo para listar los hoteles creados
     * @return Array
     */
    public function getDataHotel() {
        $sql = 'SELECT * FROM decameron.hotel';
        return $this->db->query($sql)->result();
    }

    /**
     * Metodo para listar los tipos de habitacion del hotel
     * @return type
     */
    public function getDataTypeRoom() {
        $sql = 'SELECT * FROM decameron.habitacion';
        return $this->db->query($sql)->result();
    }

    /**
     * Metodo para listar las acomodaciones 
     * @return type
     */
    public function getDataAcomodacion($id_hab) {
        $sql = 'SELECT a.* 
                FROM decameron.habitacion h
                INNER JOIN decameron.aco_hab ah ON ah.id_hab = h.id_hab
                INNER JOIN decameron.acomodacion a ON a.id_acom = ah.id_acom 
                WHERE h.id_hab = ' . $id_hab . ';';
        return $this->db->query($sql)->result();
    }
    
    
    public function createHabitacionHotel($id_hotel, $id_acom, $id_hab, $cant){
        $sql = "INSERT INTO decameron.hotel_hab_acom
                (id_hotel,
                id_acom,
                id_hab,
                cant)
                VALUES
                ($id_hotel,
                $id_acom,
                $id_hab,
                $cant);";
        $this->db->query($sql);
    }
    
    public function validate_room_hotel($id_hotel, $id_acom, $id_hab){
        $sql = " SELECT *
                FROM decameron.hotel_hab_acom
                WHERE id_hotel = $id_hotel AND id_acom = $id_acom AND id_hab=$id_hab ";
        return $this->db->query($sql)->num_rows();
    }

}
