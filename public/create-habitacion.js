//Detalle de la orden montada
var solicitud = new Vue({
    el: "#create-habitacion",
    data: {
        hotels: [],
        typeRooms: [],
        acom: [],
        id_hab: 0
    },
    methods: {
        isNumber: function (evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode !== 46) {
                evt.preventDefault();
            } else {
                return true;
            }
        },
        is_alfanumeric: function (evt) {
            var regex = new RegExp("^[a-zA-Z0-9 ]+$");
            var key = String.fromCharCode(!evt.charCode ? evt.which : evt.charCode);
            if (!regex.test(key)) {
                evt.preventDefault();
                return false;
            }
        },
        validate_string: function (e) {
            var value = e.target.value;
            document.getElementById(e.target.id).value = value.replace(/[^a-z0-9\s]/gi, '');

        },
        validate_numeric: function (e) {
            var value = e.target.value;
            document.getElementById(e.target.id).value = value.replace(/[^0-9]/gi, '');

        },
        validate_colombian_phone: function (e) {
            var length_phone = document.getElementById(e.target.id).value.length;
            if (length_phone !== 7) {
                if (length_phone !== 10) {
                    alertify.error("Valor no permitido: " + document.getElementById(e.target.id).value + " <br/> Solo se permiten números con 7 o 10 caracteres");
                    document.getElementById(e.target.id).value = '';
                }
            }


        },
        validate_phone_input: function (e) {
            this.validate_numeric(e);
            this.validate_colombian_phone(e);
        },
        get_acomodacion: function () {
            /* Cargar los tipos de habitacion */
            this.$http.get(ruta_proyecto + 'index.php/get_acomodacion/' + this.id_hab).then(function (res) {
                var arreglo = [];
                res.body.datos.forEach(function (element) {
                    arreglo.push({"text": element.nom_acom, "value": element.id_acom});
                });
                this.acom = arreglo;
            }, function (response) {
                // error callback
                console.log('error tipos de acomodacion');
            });
        }
    },

    filters: {
        formatNumber: function (value) {
            if (value != 0) {
                //var numeral = require("numeral");
                return numeral(value).format("0.0");
            }

        },
        formatMoney: function (value) {
            if (value != 0) {
                //var numeral = require("numeral");
                return numeral(value).format("0,0");
            }

        }
    },
    mounted: function () {
        sessionStorage.clear();

        /* Cargar los departamentos */
        this.$http.get(ruta_proyecto + 'index.php/get_hotel/').then(function (res) {
            var arreglo = [];
            res.body.datos.forEach(function (element) {
                arreglo.push({"text": element.nom_hotel, "value": element.idhotel});
            });
            this.hotels = arreglo;
        }, function (response) {
            // error callback
            console.log('error hoteles');
        });

        /* Cargar los tipos de habitacion */
        this.$http.get(ruta_proyecto + 'index.php/get_habitacion/').then(function (res) {
            var arreglo = [];
            arreglo.push({"text": '---Seleccione---', "value": ''});
            res.body.datos.forEach(function (element) {
                arreglo.push({"text": element.nom_hab, "value": element.id_hab});
            });
            this.typeRooms = arreglo;
        }, function (response) {
            // error callback
            console.log('error tipos de habitacion');
        });
    }
});